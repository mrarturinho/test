from django.conf.urls import url, include
from django.contrib import admin
from docs import views

urlpatterns = [
    url(r'^$', views.index, name='index'),
    url(r'^index/', views.index, name='index'),
    url(r'^organizations/', views.docs_organizations, name='docs_organizations'),
    url(r'^teams/', views.docs_teams, name='docs_teams'),
    url(r'^projects/', views.docs_projects, name='docs_projects'),
    url(r'^tasks/', views.docs_tasks, name='docs_tasks'),
]
