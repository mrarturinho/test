# ----------------------------------------------------------------------
#
# ----------------------------------------------------------------------
# Copyright (C) 2017-2017 The CCBI Project
# See LICENSE for details
# ----------------------------------------------------------------------

# python modules
# django modules
# project modules

from django.shortcuts import render


def index(request):

    return render(request, 'docs/index.html', locals())


def docs_organizations(request):

    return render(request, 'docs/organizations.html', locals())


def docs_teams(request):

    return render(request, 'docs/teams.html', locals())


def docs_projects(request):

    return render(request, 'docs/projects.html', locals())


def docs_tasks(request):

    return render(request, 'docs/tasks.html', locals())
