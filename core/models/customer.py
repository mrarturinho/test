# -*- coding: utf-8 -*-
# !/usr/bin/python
# ----------------------------------------------------------------------
# 
# ----------------------------------------------------------------------
# Copyright (C) 2017-2017 CCBI Project
# See LICENSE for details
# ----------------------------------------------------------------------

# django modules
from django.db import models
from django.utils.translation import ugettext as _
# project modules
from core.models.base import CreatedDeletedModel


class Customer(CreatedDeletedModel):

    class Meta:
        db_table = "core_customer"
        verbose_name = "Customer"
        verbose_name_plural = "Customers"

    name = models.CharField(
        max_length=255, verbose_name=_('Name'))

    def __str__(self):
        return '{}'.format(self.name)

    @staticmethod
    def create(name):
        customer = Customer(
            name=name)
        customer.save()
        return customer

    def remove(self):
        try:
            self.delete()
            return True
        except:
            return False

    def to_dict(self):
        r = dict()
        r['id'] = self.pk
        r['name'] = self.name
        return r
