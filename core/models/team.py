# ----------------------------------------------------------------------
#
# ----------------------------------------------------------------------
# Copyright (C) 2017-2017 The CCBI Project
# See LICENSE for details
# ----------------------------------------------------------------------

# python modules
# django modules
from django.db import models
from django.utils.translation import ugettext as _
# project modules
from core.models.base import CreatedDeletedModel


class Team(CreatedDeletedModel):

    class Meta:
        db_table = "core_team"
        verbose_name = "Team"
        verbose_name_plural = "Teams"

    name = models.CharField(max_length=255, verbose_name=_('Team'))
    owner = models.ForeignKey(to='core.User', verbose_name=_('Owner'))
    users = models.ManyToManyField(
        'core.User', through='TeamUser', related_name='m2m_team_user')
    organization = models.ForeignKey(
        to='core.Organization', null=True, verbose_name=_('Organization'))

    def __str__(self):
        return '{} - {}'.format(self.owner.name, self.name)

    @staticmethod
    def create(user, name, organization):
        team = Team(
            name=name,
            owner=user,
            organization=organization
        )
        team.save()
        # TODO: необходимо сделать get_owner_role
        owner_role = TeamUserRole.get_owner_role_id()
        team_user = TeamUser(
            user=user,
            team=team,
            role=owner_role
        )
        team_user.save()
        team.teamuser_set.add(team_user)
        team.save()
        return team

    def change_owner(self, user):
        if not self.organization.is_user_in_organization(user):
            return False
        try:
            self.owner = user
            self.save()
            return True
        except:
            return False

    def remove(self):
        try:
            projects = Project.get_all_by_team(self)
            for project in projects:
                project.remove()

            for teamuser in self.teamuser_set.all():
                teamuser.remove()

            self.delete()
            return True
        except:
            return False

    @staticmethod
    def is_managed(team, user):
        # TODO: test it!
        if team.is_user_in_team(user):
            team_user = TeamUser.get_by_user_team(user, team)
            role_user = TeamUserRole.get_manager_role_id()
            if team_user.role.pk >= role_user:
                return True
            else:
                return False
        else:
            return False

    @staticmethod
    def is_ruled(team, user):
        if team.is_user_in_team(user):
            team_user = TeamUser.get_by_user_team(user, team)
            role_user_id = TeamUserRole.get_owner_role_id()
            role_user = TeamUserRole.objects.get(pk=role_user_id)
            if team_user.role.pk >= role_user.pk:
                return True
            else:
                return False
        else:
            return False

    @staticmethod
    def get_all_user_teams(user):
        return Team.objects.filter(teamuser__user=user)

    def add_to_team(self, user):
        if self.is_user_in_team(user):
            return False

        user_role_id = TeamUserRole.get_user_role_id()
        teamuser = TeamUser(
            team=self,
            user=user,
            role_id=user_role_id
        )
        teamuser.save()
        self.teamuser_set.add(teamuser)
        self.save()
        return teamuser

    def is_user_in_team(self, user):
        try:
            Team.objects.get(teamuser__user=user, pk=self.pk)
            return True
        except models.ObjectDoesNotExist:
            return False

    def to_dict(self):
        team_dict = {
            'organization_id': self.organization.pk,
            'id': self.pk,
            'name': self.name,
            'owner': self.owner.to_dict(),
            'members': [member.to_dict() for member in self.teamuser_set.all()]
        }
        return team_dict

# Avoid circular references
from .teamuserrole import TeamUserRole
from .teamuser import TeamUser
from .project import Project
