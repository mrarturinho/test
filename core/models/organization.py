# ----------------------------------------------------------------------
#
# ----------------------------------------------------------------------
# Copyright (C) 2017-2017 The CCBI Project
# See LICENSE for details
# ----------------------------------------------------------------------

# python modules
# django modules
from django.db import models
from django.utils.translation import ugettext as _
# project modules
from core.models.base import CreatedDeletedModel


class Organization(CreatedDeletedModel):

    class Meta:
        db_table = "core_organization"
        verbose_name = "Organization"
        verbose_name_plural = "Organizations"

    name = models.CharField(max_length=255, verbose_name=_('Name'))
    owner = models.ForeignKey(to='core.User', verbose_name=_('Owner'))
    users = models.ManyToManyField(
        'core.User', through='OrganizationUser', related_name='m2m_organization_user')

    def __str__(self):
        return '{} @ {}'.format(self.name, self.owner.name)

    @staticmethod
    def create(user, name):
        organization = Organization(
            owner=user,
            name=name
        )
        organization.save()
        organization_user = OrganizationUser(
            organization=organization,
            user=user
        )
        organization_user.save()
        organization.organizationuser_set.add(organization_user)
        organization.save()
        return organization

    def change(self, user, name):
        if not Organization.is_ruled(self, user):
            return False

        self.name = name
        self.save()
        return True

    def remove(self):
        try:
            # TODO: REFACTOR THIS!!!
            teams = Team.objects.filter(organization=self)
            for team in teams:
                team.remove()
            self.delete()
            return True
        except:
            return False

    def is_user_in_organization(self, user):
        try:
            Organization.objects.get(organizationuser__user=user, pk=self.pk)
            return True
        except models.ObjectDoesNotExist:
            return False

    def is_owner(self, user):
        if self.owner.pk == user.pk:
            return True

        return False

    def add_member(self, user):
        if self.is_user_in_organization(user):
            return False

        organization_user = OrganizationUser(
            organization=self,
            user=user
        )
        organization_user.save()
        self.organizationuser_set.add(organization_user)
        self.save()
        return organization_user

    @staticmethod
    def is_ruled(organization, user):
        return organization.is_owner(user)

    @staticmethod
    def get_all_by_user(user):
        return Organization.objects.filter(organizationuser__user=user)

    def to_dict(self):
        organization_dict = {
            'id': self.pk,
            'name': self.name,
            'owner': self.owner.to_dict(),
            'members': [user.to_dict() for user
                        in self.organizationuser_set.all()]
        }
        return organization_dict


# Avoid circular references
from core.models.organizationuser import OrganizationUser
from core.models.team import Team
