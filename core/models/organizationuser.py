# ----------------------------------------------------------------------
#
# ----------------------------------------------------------------------
# Copyright (C) 2017-2017 The CCBI Project
# See LICENSE for details
# ----------------------------------------------------------------------

# python modules
# django modules
from django.db import models
from django.utils.translation import ugettext as _
# project modules
from core.models.base import CreatedDeletedModel


class OrganizationUser(CreatedDeletedModel):

    class Meta:
        db_table = "core_organization_user"
        verbose_name = "Organization user"
        verbose_name_plural = "Organization users"

    user = models.ForeignKey(to='core.User',  verbose_name=_("User"))
    organization = models.ForeignKey(
        to='core.Organization', verbose_name=_("Organization"))

    def __str__(self):
        return '{} - {}'.format(self.organization.name, self.user.name)

    @staticmethod
    def get_by_organization_user(organization, user):
        try:
            return OrganizationUser.objects.get(
                organization=organization,
                user=user
            )
        except models.ObjectDoesNotExist:
            return None
        except:
            return False

    def remove(self):
        try:
            user = self.user
            # TODO: refactor this!
            teamusers = TeamUser.objects.filter(user=user)
            for teamuser in teamusers:
                teamuser.remove()

            self.delete()
            return True
        except:
            return False

    def to_dict(self):
        organization_user_dict = {
            'id': self.pk,
            'user': self.user.to_dict(),
            'organization_id': self.organization.pk
        }
        return organization_user_dict


# Avoid circular references
from core.models.teamuser import TeamUser
