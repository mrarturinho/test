# ----------------------------------------------------------------------
#
# ----------------------------------------------------------------------
# Copyright (C) 2017-2017 The CCBI Project
# See LICENSE for details
# ----------------------------------------------------------------------

# python modules
# django modules
from django.db import models
# project modules
from core.utils import get_now_with_tz


class NonDeletedManager(models.Manager):
    """ Base manager, returns non-deleted objects """

    def get_queryset(self):
        """ return all items without deleted """
        return super(NonDeletedManager, self).get_queryset().filter(deleted_at=None)


class WithDeletedManager(models.Manager):
    """ Manage deleted objects (deleted_at is not null) """

    def get_queryset(self):
        """ return all items INCLUDING deleted """
        return super(WithDeletedManager, self).get_queryset()


class CreatedDeletedModel(models.Model):

    class Meta:
        abstract = True

    created_at = models.DateTimeField(auto_now_add=True, blank=False, null=False)
    deleted_at = models.DateTimeField(blank=True, null=True)

    objects = NonDeletedManager()
    with_deleted = WithDeletedManager()

    def change(self, *args, **kwargs):
        try:
            for k, v in kwargs.items():
                setattr(self, k, v)

            self.save()
            return True
        except Exception as why:
            return False

    def delete(self, *args, **kwargs):
        self.deleted_at = get_now_with_tz()
        self.save()

    @classmethod
    def get_by_id(cls, object_id):
        try:
            return cls.objects.get(pk=object_id)
        except models.ObjectDoesNotExist:
            return None
        except:
            return False
