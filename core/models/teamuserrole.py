# ----------------------------------------------------------------------
#
# ----------------------------------------------------------------------
# Copyright (C) 2017-2017 The CCBI Project
# See LICENSE for details
# ----------------------------------------------------------------------

# python modules
# django modules
from django.db import models
from django.utils.translation import ugettext as _
# project modules
from core.models.base import CreatedDeletedModel


class TeamUserRole(CreatedDeletedModel):

    class Meta:
        db_table = "core_team_user_role"
        verbose_name = "Team User roles"
        verbose_name_plural = "Team user roles"

    name = models.CharField(max_length=255, verbose_name=_('Name'))

    def __str__(self):
        return self.name

    @staticmethod
    def get_user_role_id():
        return 0

    @staticmethod
    def get_manager_role_id():
        return 1

    @staticmethod
    def get_owner_role_id():
        # return 2
        # raise DeprecationWarning ??
        try:
            owner_role = TeamUserRole.objects.get(name="Owner")
            return owner_role
        except models.ObjectDoesNotExist:
            owner_role = TeamUserRole(
                name="Owner"
            )
            owner_role.save()
            return owner_role

    @staticmethod
    def get_admin_role_id():
        return 3

    def to_dict(self):
        return {
            'id': self.pk,
            'name': self.name
        }
