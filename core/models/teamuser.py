# ----------------------------------------------------------------------
#
# ----------------------------------------------------------------------
# Copyright (C) 2017-2017 The CCBI Project
# See LICENSE for details
# ----------------------------------------------------------------------

# python modules
# django modules
from django.db import models
from django.utils.translation import ugettext as _
# project modules
from core.models.base import CreatedDeletedModel


class TeamUser(CreatedDeletedModel):

    class Meta:
        db_table = "core_team_user"
        verbose_name = "Team user"
        verbose_name_plural = "Team users"

    user = models.ForeignKey(
        to='core.User', verbose_name=_("User"))
    team = models.ForeignKey(
        to='core.Team', verbose_name=_("Team"))
    role = models.ForeignKey(
        to='core.TeamUserRole', default=0)
    hour_tax = models.FloatField(
        default=0, blank=True, verbose_name=_("Hour Tax"))
    hour_tax_with_maintain = models.FloatField(
        default=0, blank=True, verbose_name=_("Hour Tax with maintain"))
    hour_tax_with_profitability = models.FloatField(
        default=0, blank=True, verbose_name=_("Hour Tax with PF"))
    month_hours = models.FloatField(
        default=0, blank=True, verbose_name="Month hours")

    def __str__(self):
        return '{} - {}'.format(self.team.name,
            self.user.name and self.user.name or self.user.email)

    def change(self, hour_tax, hour_tax_with_maintain, hour_tax_with_profitability, month_hours, role_id):
        try:
            if not isinstance(role_id, int):
                raise ValueError
            # TODO: переделать role_id в role
            self.hour_tax = hour_tax
            self.hour_tax_with_maintain = hour_tax_with_maintain
            self.hour_tax_with_profitability = hour_tax_with_profitability
            self.month_hours = month_hours
            self.role_id = role_id
            self.save()
            return True
        except ValueError:
            return False
        except AttributeError:
            return False

    def remove(self):
        team = self.team
        user = self.user
        if user == team.owner:
            return False
        try:
            tasks = Task.get_all_by_user(user)
            for task in tasks:
                task.assignee = None
                task.save()

            self.delete()
            return True
        except ValueError:
            return False
        except AttributeError:
            return False
        except models.ObjectDoesNotExist:
            return False

    @staticmethod
    def get_by_user_team(user, team):
        try:
            return TeamUser.objects.get(team=team, user=user)
        except models.ObjectDoesNotExist:
            return None
        except:
            return False

    def to_dict(self):
        team_user_dict = {
            'id': self.pk,
            'user': self.user.to_dict(),
            'team_id': self.team.pk,
            'hour_tax': self.hour_tax,
            'hour_tax_with_maintain': self.hour_tax_with_maintain,
            'hour_tax_with_profitability': self.hour_tax_with_profitability,
            'month_hours': self.month_hours,
            'role': self.role.to_dict()
        }
        return team_user_dict

# Avoid circular references
from core.models.task import Task
