# ----------------------------------------------------------------------
#
# ----------------------------------------------------------------------
# Copyright (C) 2017-2017 The CCBI Project
# See LICENSE for details
# ----------------------------------------------------------------------

# python modules
# django modules
from django.db import models
from django.utils.translation import ugettext as _
# project modules
from core.models.base import CreatedDeletedModel
from core.utils import hash_password, create_token, get_now_with_tz, \
    validate_email, check_raw_password


class User(CreatedDeletedModel):
    class Meta:
        db_table = "core_users"
        verbose_name = "User"
        verbose_name_plural = "Users"

    email = models.CharField(
        max_length=255, verbose_name=_('E-email'))
    name = models.CharField(
        max_length=100, null=True, blank=True, verbose_name=_('Name'))
    password = models.CharField(
        max_length=255, null=True, blank=True, verbose_name=_('Password'))
    invite_token = models.CharField(
        max_length=255, unique=True, null=True,
        blank=True, verbose_name=_('Invite Token'))
    email_token = models.CharField(
        max_length=255, unique=True, null=True,
        blank=True, verbose_name=_('Email Confirmation Token'))
    administrator = models.BooleanField(
        default=False, blank=True, verbose_name=_('Administrator'))
    register_submitted = models.BooleanField(
        default=False, blank=True, verbose_name=_('Register submitted'))
    reset_password_token = models.CharField(
        max_length=255, null=True, blank=True,
        verbose_name=_('Reset password token'))
    asana_user_id = models.CharField(
        max_length=255, null=True, blank=True, verbose_name=_('Asana User Id'))

    def __str__(self):
        return '{} - {}'.format(self.email, self.name and self.name or '')

    def set_password(self, raw):
        """
        Set hashed password to this user from raw
        :param raw:
        :type raw: str
        :return:
        """
        self.password = hash_password(raw)
        self.save()

    def set_invite_token(self):
        """
        Set invite confirm token to this User
        :return:
        """
        self.invite_token = create_token()

    def set_email_token(self):
        """
        Set email confirm token to this User
        :return:
        """
        self.email_token = create_token()

    def set_reset_password_token(self):
        self.reset_password_token = create_token()
        self.save()

    @staticmethod
    def check_email_unique(email):
        if not validate_email(email):
            return False

        users = User.objects.all().filter(email=email)
        if len(users) > 0:
            return False
        else:
            return True

    @staticmethod
    def register(name, email, asana_id=None, password='54321'):
        # TODO: password is deprecated
        if not validate_email(email):
            return False

        if not User.check_email_unique(email=email):
            return False

        user = User(
            name=name,
            email=email,
            asana_user_id=asana_id
        )
        user.set_password(password)
        user.set_email_token()
        user.register_submitted = False
        user.save()
        return user

    @staticmethod
    def is_exist(email):
        raise DeprecationWarning
        try:
            User.objects.get(email=email)
            return True
        except ValueError:
            return False
        except models.ObjectDoesNotExist:
            return False

    @staticmethod
    def is_exist_invited(invite_token):
        try:
            User.objects.get(invite_token=invite_token)
            return True
        except ValueError:
            return False
        except models.ObjectDoesNotExist:
            return False

    @staticmethod
    def is_exist_registered(email_token):
        try:
            User.objects.get(email_token=email_token)
            return True
        except ValueError:
            return False
        except models.ObjectDoesNotExist:
            return False

    @staticmethod
    def register_approve(email_token):
        try:
            user = User.objects.get(email_token=email_token)
        except models.ObjectDoesNotExist:
            return None
        except ValueError:
            return False
        user.register_submitted = True
        user.email_token = None
        user.save()
        return user

    @staticmethod
    def is_exist_reset_password_token(reset_password_token):
        try:
            User.objects.get(reset_password_token=reset_password_token)
            return True
        except models.ObjectDoesNotExist:
            return False
        except:
            return False

    @staticmethod
    def get_by_reset_password_token(reset_password_token):
        try:
            return User.objects.get(reset_password_token=reset_password_token)
        except models.ObjectDoesNotExist:
            return None
        except:
            return False

    @staticmethod
    def invite(email):
        if User.check_email_unique(email=email):
            user = User(
                email=email
            )
            user.set_invite_token()
            user.save()
            return user
        else:
            return False

    @staticmethod
    def invite_approve(invite_token, name, password):
        try:
            user = User.objects.get(invite_token=invite_token)
        except models.ObjectDoesNotExist:
            return None
        except ValueError:
            return False
        user.name = name
        user.set_password(password)
        user.invite_token = None
        user.email_token = None
        user.register_submitted = True
        user.save()
        return user

    @staticmethod
    def login(email, password, type):
        try:
            user = User.objects.get(email=email)
        except ValueError:
            return False
        except models.ObjectDoesNotExist:
            return False

        if not check_raw_password(raw=password, hashed=user.password):
            return False

        token = AuthToken.create(user, 'web')
        return user, token

    @staticmethod
    def auth(token):
        try:
            user = User.objects.get(auth_token=token)
        except ValueError:
            return False
        except models.ObjectDoesNotExist:
            return False

        # TODO: сделать проверку времени жизни токена
        return user

    @staticmethod
    def get_by_email(email):
        if not validate_email(email):
            return False
        try:
            return User.objects.get(email=email)
        except models.ObjectDoesNotExist:
            return None
        except:
            return False

    def to_dict(self):
        return {
            'id': self.pk,
            'name': self.name and self.name or '',
            'email': self.email
        }


# Avoid circular references
from core.models.authtoken import AuthToken
