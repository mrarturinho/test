# ----------------------------------------------------------------------
#
# ----------------------------------------------------------------------
# Copyright (C) 2017-2017 The CCBI Project
# See LICENSE for details
# ----------------------------------------------------------------------

# django modules
from django.db import models
from django.utils.translation import ugettext as _
from django.core.validators import MaxValueValidator, MinValueValidator
# project modules
from core.models.base import CreatedDeletedModel
from core.utils import get_now_with_tz
from core.models.task import Task
from core.enums import TaskPaymentType, NonBillablePaymentType, TaskStateType,\
    ExpenseType


STATE_CHOICES = (
    (TaskStateType.ACTIVE, 'Active'),
    (TaskStateType.PAUSED, 'Paused'),
    (TaskStateType.DELETED, 'Deleted'),
    (TaskStateType.IN_ARCHIVE, 'In archive'),
)

TASK_PAYMENT_CHOICES = (
    (TaskPaymentType.BY_ESTIMATE_HOURS, 'Pay for estimate hours'),
    (TaskPaymentType.BY_PLANNED_HOURS, 'Pay for planned hours')
)

NO_BILLABLE_PAYMENT_CHOICES = (
    (NonBillablePaymentType.PAY, 'Pay'),
    (NonBillablePaymentType.DONT_PAY, 'Don\'t pay')
)


class Project(CreatedDeletedModel):

    class Meta:
        db_table = "core_project"
        verbose_name = "Project"
        verbose_name_plural = "Projects"

    name = models.CharField(
        max_length=255, verbose_name=_('Name'))
    team = models.ForeignKey(
        to='core.Team', verbose_name=_('Team'))
    start_date = models.DateField(
        null=True, blank=True, verbose_name=_('Start date'))
    due_date = models.DateField(
        null=True, blank=True, verbose_name=_('Due date'))
    state = models.IntegerField(
        choices=STATE_CHOICES, default=1, verbose_name=_('State'))
    manager = models.ForeignKey(
        to='core.TeamUser', null=True, blank=True, related_name='Manager',
        verbose_name=_("Manager"))
    customer = models.ForeignKey(
        to='core.Customer', null=True, blank=True, related_name='Customer',
        verbose_name=_("Customer"))
    notes = models.TextField(
        null=True, blank=True, max_length=2000, verbose_name=_('Notes'))
    budget = models.FloatField(
        blank=True, default=0, validators=[MinValueValidator(0)],
        verbose_name=_('Budget'))
    project_bonus = models.FloatField(
        blank=True, default=0,
        validators=[MinValueValidator(0), MaxValueValidator(100)],
        verbose_name=_('Project bonus'))
    task_bonus = models.FloatField(
        blank=True, default=0,
        validators=[MinValueValidator(0), MaxValueValidator(100)],
        verbose_name=_('Task bonus'))
    task_payment_type = models.IntegerField(
        choices=TASK_PAYMENT_CHOICES, default=0,
        verbose_name=_('Task payment type'))
    non_billable_payment_type = models.IntegerField(
        choices=NO_BILLABLE_PAYMENT_CHOICES, default=0,
        verbose_name=_('Non-billable payment type'))

    def __str__(self):
        return '{} - {}'.format(self.team.name, self.name)

    @staticmethod
    def create(team, name=''):
        project = Project(
            team=team,
            name=name,
            start_date=get_now_with_tz()
        )
        project.save()
        return project

    def remove(self):
        try:
            tasks = Task.get_all_by_project(self)
            for task in tasks:
                task.remove()

            # TODO after remove other tasks
            self.delete()
            return True
        except:
            return False

    @staticmethod
    def get_all_by_organization(organization):
        try:
            return Project.objects.filter(team__organization=organization)
        except:
            return False

    @staticmethod
    def get_all_by_team(team):
        try:
            return Project.objects.filter(team=team)
        except:
            return False

    @staticmethod
    def get_all_my(user):
        try:
            return Project.objects.filter(team__teamuser__user=user)
        except:
            return False

    @property
    def finish_date(self):
        from django.db.models import Max
        tasks = Task.get_all_by_project(self)
        result = tasks.aggregate(Max('due_date'))
        return result['due_date__max']

    def to_dict(self):
        finish_date = self.finish_date
        project_dict = {
            'id': self.pk,
            'name': self.name,
            'team_id': self.team.pk,
            'start_date': self.start_date and self.start_date or '',
            'due_date': self.due_date and self.due_date or '',
            'finish_date': finish_date and finish_date or '',
            'state': self.state,
            'manager': self.manager and self.manager.user.to_dict() or None,
            'customer': self.customer and self.customer.to_dict() or None,
            'notes': self.notes and self.notes or '',
            'budget': self.budget,
            'project_bonus': self.project_bonus,
            'task_bonus': self.task_bonus,
            'task_payment_type': self.task_payment_type,
            'non_billable_payment_type': self.non_billable_payment_type
        }
        return project_dict
