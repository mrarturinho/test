# ----------------------------------------------------------------------
#
# ----------------------------------------------------------------------
# Copyright (C) 2017-2017 The CCBI Project
# See LICENSE for details
# ----------------------------------------------------------------------

# python modules
import datetime
# django modules
from django.db import models
from django.utils.translation import ugettext as _
# project modules
from core.models.base import CreatedDeletedModel
from core.utils import get_now_with_tz


class BaseTask(CreatedDeletedModel):

    class Meta:
        abstract = True

    project = models.ForeignKey(to='core.Project')
    name = models.CharField(max_length=255, verbose_name=_('Name'))
    assignee = models.ForeignKey(
        to='core.TeamUser', blank=True, null=True, verbose_name=_('Assignee'))
    start_date = models.DateField(blank=True, verbose_name=_('Start date'))
    due_date = models.DateField(
        null=True, blank=True, verbose_name=_('Due date'))
    completed_date = models.DateField(
        null=True, blank=True, verbose_name=_('Completed date'))

    def __str__(self):
        return '{} - {}'.format(self.project.name, self.name)

    @classmethod
    def create(cls, project, name):
        try:
            object = cls(
                project=project,
                name=name,
                start_date=get_now_with_tz()
            )
            object.save()
            return object
        except Exception as why:
            return False

    def remove(self):
        try:
            self.delete()
            return True
        except:
            return False

    @property
    def is_completed(self):
        return not (self.completed_date is None)

    @property
    def is_overdue(self):
        due_date = self.due_date
        if not due_date:
            return False

        # TODO change date to datetime
        now = get_now_with_tz().date()

        # TODO !!! change EVERYWHERE for models date to datetime !!!
        if isinstance(due_date, datetime.datetime):
            due_date = due_date.date()

        return now > due_date

    @classmethod
    def get_all_by_project(cls, project):
        try:
            return cls.objects.filter(project=project)
        except models.ObjectDoesNotExist:
            return None
        except:
            return False

    @classmethod
    def get_all_by_user(cls, user):
        try:
            return cls.objects.filter(assignee__user=user)
        except:
            return False

    def to_dict(self):
        r = dict()
        r['id'] = self.pk
        r['project_id'] = self.project.pk
        r['name'] = self.name
        r['assignee'] = self.assignee and self.assignee.user.to_dict() or None
        r['start_date'] = self.start_date and self.start_date or ''
        r['due_date'] = self.due_date and self.due_date or ''
        r['completed_date'] = self.completed_date and self.completed_date or ''
        return r


class Task(BaseTask):

    class Meta:
        db_table = "core_task"
        verbose_name = "Task"
        verbose_name_plural = "Tasks"

    non_billable = models.BooleanField(blank=True, default=False,
                                       verbose_name='Non billable')
    hour_rate = models.FloatField(blank=True, default=0.0,
                                  verbose_name=_('Hour rate'))
    hours_planned = models.FloatField(blank=True, default=0.0,
                                      verbose_name=_('Hours planned'))
    hours_spent = models.FloatField(blank=True, default=0.0,
                                    verbose_name=_('Hours spent'))
    locked = models.BooleanField(blank=True, default=False,
                                 verbose_name='Locked')

    @property
    def hours_remain(self):
        result = self.hours_planned - self.hours_spent
        result = result if result > 0 else 0
        return result

    @property
    def progress(self):
        if self.is_completed:
            return 100

        if self.hours_planned == 0:
            return 0

        result = self.hours_spent/self.hours_planned*100
        result = round(result, 2)
        if result > 80:
            return 80

        return result

    @property
    def budget(self):
        if self.non_billable:
            return 0

        return self.hour_rate * self.hours_planned

    @property
    def profit(self):
        return 0

    def to_dict(self):
        r = super().to_dict()
        r['non_billable'] = self.non_billable
        r['hour_rate'] = self.hour_rate
        r['hours_planned'] = self.hours_planned
        r['hours_spent'] = self.hours_spent
        r['hours_remain'] = self.hours_remain
        r['progress'] = self.progress
        r['budget'] = self.budget
        r['profit'] = self.profit
        r['locked'] = self.locked
        return r
