# python modules
# django modules
from django.db import models
from django.utils.translation import ugettext as _
# project modules
from core.models.base import CreatedDeletedModel
from core.utils import create_token, get_now_with_tz


class AuthToken(CreatedDeletedModel):
    class Meta:
        db_table = "core_auth"
        verbose_name = "Auth"
        verbose_name_plural = "Auths"

    user = models.ForeignKey(
        to='core.User', verbose_name=_("User"))
    token = models.CharField(
        max_length=1024, verbose_name=_("Token"))
    expired_by = models.DateTimeField(
        max_length=255, null=True, blank=True, verbose_name=_('Token expired date'))
    client = models.CharField(
        max_length=255, null=True, blank=True, verbose_name=_('Api client type'))

    @staticmethod
    def create(user, client_type='web'):
        token = AuthToken(
            user=user,
            token=create_token(),
            expired_by=get_now_with_tz(),
            client=client_type
        )
        token.save()
        return token

    @staticmethod
    def get_user_by_token(token, client_type):
        try:
            token = AuthToken.objects.get(token=token, client=client_type)
            return token.user
        except models.ObjectDoesNotExist:
            return None
        except ValueError:
            return False

    def to_dict(self):
        return {
            'token': self.token,
            'expired_by': self.expired_by
        }
