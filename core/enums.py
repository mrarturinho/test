# ----------------------------------------------------------------------
#
# ----------------------------------------------------------------------
# Copyright (C) 2017-2017 The CCBI Project
# See LICENSE for details
# ----------------------------------------------------------------------

# python modules
# django modules
# project modules


class ExpenseType:
    PLANNED = 0
    UNPLANNED = 1


class TaskStateType:
    ACTIVE = 0
    PAUSED = 1
    DELETED = 2
    IN_ARCHIVE = 3


class TaskPaymentType:
    BY_ESTIMATE_HOURS = 0
    BY_PLANNED_HOURS = 1


class NonBillablePaymentType:
    PAY = 0
    DONT_PAY = 1
