import datetime
import hashlib
from django.http import JsonResponse, HttpResponse
from django.core.serializers.json import DjangoJSONEncoder
import json
from django.conf import settings
from pytz import timezone


def get_default_tz():
    """" returns current TZ according to settings """
    tz = timezone(settings.TIME_ZONE)
    return tz


def get_now_with_tz():
    """" returns current date-time with default timezone """
    tz = get_default_tz()
    return datetime.datetime.now(tz)


def convert_str_to_date(date_string):
    try:
        return datetime.datetime.strptime(date_string, "%Y-%m-%dT%H:%M:%S.%f")
    except:
        return False


def hash_password(raw_password):
    """ encode raw password with sha256 """
    hh = hashlib.new('sha256', settings.SECRET_KEY.encode('utf-8'))
    hh.update(raw_password.encode('utf-8'))
    return hh.hexdigest()


def create_token():
    import uuid
    token = uuid.uuid4().__str__()
    return token


def check_raw_password(raw, hashed):
    """ check if raw password equal its hashed repr """
    h = hash_password(raw)
    return hashed == h


def response_success(**kwargs):
    """
    Return Json response object with success statement
    :param kwargs:
    :return: django.http.JsonResponse
    """
    kwargs.update({'status': 'success'})
    return JsonResponse(kwargs)


def response_error(message, **kwargs):
    """
    Return Json response object with error statement
    :param message: message while error
    :type message: str
    :param kwargs:
    :return: django.http.JsonResponse
    """
    kwargs.update({
        'status': 'error',
        'message': message
    })
    return JsonResponse(kwargs)


def response_json_list(object_list):
    """
    Return Json response object with success statement
    :param object_list:
    :type object_list: list
    :return: django.http.JsonResponse
    """
    list_json = json.dumps(list(object_list), cls=DjangoJSONEncoder)
    return HttpResponse(
        list_json,
        content_type='application/javascript; charset=utf8'
    )


def validate_email(email):
    from django.core.validators import validate_email
    from django.core.exceptions import ValidationError
    try:
        validate_email(email)
        return True
    except ValidationError:
        return False
