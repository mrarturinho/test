# ----------------------------------------------------------------------
#
# ----------------------------------------------------------------------
# Copyright (C) 2017-2017 The CCBI Project
# See LICENSE for details
# ----------------------------------------------------------------------

# python modules
# django modules
from django.contrib import admin
# project modules
from core.models import AuthToken, User, Organization, OrganizationUser,\
    Team, TeamUser, TeamUserRole, Project, Task, Customer


admin.site.register(AuthToken)
admin.site.register(User)
admin.site.register(Organization)
admin.site.register(OrganizationUser)
admin.site.register(TeamUserRole)
admin.site.register(Team)
admin.site.register(TeamUser)
admin.site.register(Project)
admin.site.register(Task)
admin.site.register(Customer)
