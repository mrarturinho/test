from django.test import TestCase
from core.models import User, Team, Organization


class AllSetUp(TestCase):
    def setUp(self):
        self.test_user1 = User.register(
            name="testuser1",
            email="testuser1@testmail.com",
            asana_id="1234567891",
            password="zadxsfcdg1",
        )
        self.test_user2 = User.register(
            name="testuser2",
            email="testuser2@testmail.com",
            asana_id="1234567892",
            password="zadxsfcdg2",
        )
        self.test_org1 = Organization.create(
            user=self.test_user1,
            name="testorg1"
        )
        self.test_org1.add_member(self.test_user2)
        self.test_team1 = Team.create(
            user=self.test_user1,
            name="MyTeam",
            organization=self.test_org1
        )

    def test_create_user(self):
        self.assertIsInstance(self.test_user1, User)

    def test_exist_registered_user(self):
        self.assertTrue(self.test_user2.is_exist_registered)

    def test_user_is_owner_for_org1(self):
        self.assertTrue(self.test_org1.is_owner(self.test_user1))

    def test_user_is_owner_for_org2(self):
        self.assertFalse(self.test_org1.is_owner(self.test_user2))

    def test_user_in_org1(self):
        self.assertTrue(self.test_org1.is_user_in_organization(self.test_user1))

    def test_user_in_org2(self):
        self.assertTrue(self.test_org1.is_user_in_organization(self.test_user2))

    def test_user_in_team1(self):
        self.assertTrue(self.test_team1.is_user_in_team(self.test_user1))

    def test_user_in_team2(self):
        self.assertFalse(self.test_team1.is_user_in_team(self.test_user2))
